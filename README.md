# desafio_inmetrics

Versões utilizadas:
------------------------
<ul>
  <li>ChromeDriver 84.0.4147.30</li>
  <li>Ruby: ruby 2.5.8p224</li>
</ul>

Gems utilizadas:
-----------------
<ul>
	<li>capybara</li>
	<li>cucumber</li>
	<li>rspec</li>
	<li>selenium-webdriver</li>
	<li>site_prism</li>
	<li>pry</li>
	<li>faker</li>
	<li>httparty</li>
<ul>
<br />

Estrutura de pastas do projeto
------------------------------
Features<br />
  **page_objects:** Pasta definida para armazenar as pages objects referente a automação web e api.<br />
  **api:** Esta pasta tem a função de armazenar os arquivos com os principais métodos das apis.<br />
  **page:** O objetivo dessa pasta é armazenar as funções que serão utilizadas para a automação web.<br />


  <br />
  
  api: pasta principal para armazenar as features que por sua vez contém os bdds das apis.<br />
  site: pasta principal para armazer as features que por sua vez contém os bdds das páginas web.<br />
	
  **steps_definitions:**<br />
  **api:** o objetivo dessa pasta é armazenar os steps em ruby para testes de apis<br />
  **site:** o objetivo dessa pasta é armazenar os steps em ruby para testes das páginas web. <br />
	 
**Support:** Algumas pastas e arquivos foram utilizados como destacado abaixo: <br />
   **data:** contém o arquivo test_data.yaml que sua funcionalidade é armazer as massas de dados para serem utilizada nos testes<br />
 **env.rb:** este arquivo é utilizado para fazer o require das gems, e também possui a configuração do apphost e baseurl_api<br />
	     default_driver: possui a configuração do selenium_chrome
		        apphost: contém a url principal do site para realizar os testes web
		    baseurl_api: possui o link principal do serviço para realizar os testes de apis.
 <br />
 **hooks.rb:**
	Este arquivo faz a referência para as classes tanto para web quanto para api instanciando as classes no Before 
	e permitindo as mesmas serem utilizadas em qualquer parte do projeto

 
 **pasta report:** sua funcionalidade é poder armazenar os relatórios gerados durante a execução dos testes.
 
Configuração do arquivo cucumber.yaml
----------------------------------------
Esse arquivo possui as configurações para executar os testes em api e em web <br />
**default:** configuração padrão para executar teste utilizando o navegador chrome <br />
**api_log:** informa o perfil de testes que será utilizado, sendo nese caso o perfil de api <br />
**api:** contém o ENVIRONMENT_TYPE_API, devemos informar a tag api para executar os testes de apis <br />

Configuração para gerar relatório cucumber.yaml
-----------------------------------------------
**<%time = Time.now.strftime('%d%m%Y_%H%M%S').to_s%>** <br />
A linha acima é utilizada para pegar a data e a hora do sistema, e adicionar no nome do relatório gerado durante os testes. <br />

**html_report_api:** deve ser adicionado no final do comando de execução para gerar o relatório de testes de api <br />
O comando para executar os testes de api deve ser: **cucumber -p api -t@inmetrics_desafio_api -p html_report_api** <br />
**a configuração do cucumber é:** --out=report/report_api_<%=time%>.html <br />
Irá armazenar o relatório destacando que é um relatório de api_ mais a data e a hora que foi gerado <br />


**html_report:** possui a configuração para gerar os relatórios dos testes web. <br />
O comando para executar os testes de api deve ser: **cucumber -t@inmetrics_desafio -p html_report** <br />
Irá armazenar o relatório mais a data e a hora que foi gerado <br />
	

Execuções para API e Web
-------------------------
**API** <br/>
Tag principal para execução dos testes web: **@inmetrics_desafio_api** <br/>
Para executar os testes de api devemos informar o comando abaixo:<br/>
**cucumber -p api -t@inmetrics_desafio_api -p html_report** <br/>


**Web** <br/>
Tag principal para execução dos testes web: **@inmetrics_desafio** <br/>
Para executar os testes web devemos informar o comando abaixo: <br/>
**cucumber -t@inmetrics_desafio -p html_report**  <br/>

Devops
-------------------------
Para a integração contínua do projeto de automação, utilizei o docker e o jenkins <br />
E para executar os testes fiz a configuração na própria opção do jenkins através da opção Executar no comando do Windows <br />