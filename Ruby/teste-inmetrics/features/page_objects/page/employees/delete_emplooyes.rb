# encoding: utf-8

class DeleteEmployeesPage < SitePrism::Page
    
    element :message_success, '.alert.alert-success'
end
  