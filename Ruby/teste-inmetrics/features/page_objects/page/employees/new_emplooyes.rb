# encoding: utf-8

class NewEmployeesPage < SitePrism::Page

    element :input_name, '#inputNome'
    element :input_cpf, '#cpf'
    element :input_office, '#inputCargo'
    element :input_money, '#dinheiro'
    element :option_clt, '.radio-button'
    element :select_genre, '#slctSexo'
    element :input_admission, '#inputAdmissao'
    element :btn_send, '.cadastrar-form-btn'
    element :message_success, '.alert.alert-success'

    def fill_form_employes(data)
        input_name.set(data['nome'])
        input_office.set(data['cargo'])
        input_cpf.set(data['cpf'])
        input_money.set(data['salario'])
        select_genre.select("Masculino")
        option_clt.choose(data['tipo_contratacao'])
        input_admission.set(data['admissao'])
    end

    def click_btn_form
        btn_send.click
    end
end
  