# encoding: utf-8

class LoginPage < SitePrism::Page
  
  element :title_page, '.login100-form-title'
  element :username, 'input[name="username"]'
  element :password, 'input[name="pass"]'
  element :message_error, '.alert.alert-danger'
  element :link_register, '.txt2.bo1'
  
  def home
    visit "/"
  end

  def load_data_test
    file = YAML.load_file(File.join(Dir.pwd, "/features/support/data/test_data.yaml"))
  end

  def login_user(user_login)
    username.set(user_login["email"])
    password.set(user_login["password"])
  end

  def button_login
    click_button 'Entre'
  end

  def register
    link_register.click
  end
end
