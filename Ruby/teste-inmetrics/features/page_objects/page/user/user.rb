# encoding: utf-8

class NewUserPage < SitePrism::Page
  
  element :username, 'input[name="username"]'
  element :password, 'input[name="pass"]'
  element :confirm_password, 'input[name="confirmpass"]'
  element :message_error, '.alert.alert-danger'

  def load
    visit "/"
  end
  
  def fill_form_user
    name = Faker::Name.name
    username.set(name.gsub!(" ",""))
    password.set("123456")
    confirm_password.set("123456")
    click_button 'Cadastrar'
  end

  def fill_form_user_existent(name)
    username.set(name)
    password.set("123456")
    confirm_password.set("123456")
    click_button 'Cadastrar'
  end
end
