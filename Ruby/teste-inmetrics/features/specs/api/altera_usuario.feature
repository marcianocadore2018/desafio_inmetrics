# language:pt

@inmetrics_desafio_api
@alterar_usuario_api
Funcionalidade: Alterar Usuário
  Eu como profissional de QA
  Quero alterar o usuário
  Para que eu possa validar se o serviço está correto

  @alterar_usuario_sucesso_api
  Cenário: Alterar usuário com sucesso
    Dado que estou consultando o id do último usuário para enviar como parâmetro na alteração
    E estou informando novos dados para alteração
    Quando envio a solicitação de alteração no serviço "empregado/alterar"
    Então o retorno do serviço da alteração deverá ser 202
    
  @alterar_usuario_informacoes_invalidas_api
  Esquema do Cenário: Tentar realizar alterar usuário com informações inválidas
    Dado que estou consultando o id do último usuário para enviar como parâmetro na alteração
    E estou informando dados inválidos como "<admissao>" "<cargo>" "<comissao>" "<cpf>" "<nome>" "<salario>" "<sexo>" <departamento> "<tipocontratacao>"
    Quando envio a solicitação de alteração no serviço "empregado/alterar"
    Então a mensagem de alteração deverá ser "<mensagem>"
    E o retorno do serviço da alteração deverá ser 400
    Exemplos:
        | admissao   | cargo         | comissao   | cpf            | nome      | salario   | sexo  | departamento | tipocontratacao | mensagem                                            |
        |            | analista      | 250.00,00  | 021.346.190-07 | ana       | 3.200,00  | m     |    2         | pj              | admissao: deve estar no padrão 31/12/2020           |
        | 01/02/2020 |               | 250.00,00  | 021.346.190-07 | paulo     | 3.200,00  | m     |    1         | pj              | cargo: não utilizar números ou caracteres especiais |    
        | 01/02/2020 | desenvolvedor | 200        | 021.346.190-07 | pedro     | 3.200,00  | m     |    3         | pj              | comissao: deve estar no padrão 1.000,00             |
        | 01/02/2020 | designer      | 250.00,00  |                | jose      | 3.200,00  | m     |    4         | pj              | cpf: deve estar no padrão 111.222.333-00            |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 |           | 3.200,00  | m     |    1         | pj              | nome: não utilizar números ou caracteres especiais  |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 | helio     |           | m     |    1         | pj              | salario: deve estar no padrão 1.000,00              |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 | helio     |  4.240,00 | masc  |    1         | pj              | sexo: m, f ou i                                     |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 | helio     |  4.240,00 | m     |    0         | pj              | Empregado não cadastrado                            |
        | 01/02/2020 | gerente qa    | 250.00,00  | 021.346.190-07 | luis      | 3.200,00  | m     |    1         | pessoa física   | tipoContratacao: pj ou clt                          |  