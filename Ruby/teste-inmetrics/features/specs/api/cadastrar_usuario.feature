# language:pt

@inmetrics_desafio_api
@cadasto_usuario_api
Funcionalidade: Cadastrar usuário
  Eu como profissional de QA
  Quero realizar o cadastro do usuário via api
  Para poder validar a funcionalidade do serviço

  @cadastrar_usuario_sucesso_api
  Cenário: Cadastrar usuário com sucesso
    Dado que possuo os dados do usuário para realizar o cadastro
    Quando envio a solicitação de post para o endereço "empregado/cadastrar"
    Então o retorno do serviço deverá ser 202
    E o response deverá ter os campos "admissao" "cargo" "comissao" "cpf" "nome" "salario" "sexo" "tipoContratacao"
    E também deverei armazenar o id, nome, cpf do usuário para realizar consultas futuras

  @cadastrar_usuario_dados_invalidos_api
  Esquema do Cenário: Tentar realizar cadastro de usuário com dados inválidos
    Dado que estou enviando as dados inválidos "<admissao>" "<cargo>" "<comissao>" "<cpf>" "<nome>" "<salario>" "<sexo>" <departamento> "<tipocontratacao>"
    Quando envio a solicitação de post para o endereço "empregado/cadastrar"
    Então a mensagem deverá ser "<mensagem>"
    E o retorno do serviço deverá ser 400
    Exemplos:
        | admissao   | cargo         | comissao   | cpf            | nome      | salario   | sexo  | departamento | tipocontratacao | mensagem                                            |
        |            | analista      | 250.00,00  | 021.346.190-07 | ana       | 3.200,00  | m     |    2         | pj              | admissao: deve estar no padrão 31/12/2020           |
        | 01/02/2020 |               | 250.00,00  | 021.346.190-07 | paulo     | 3.200,00  | m     |    1         | pj              | cargo: não utilizar números ou caracteres especiais |    
        | 01/02/2020 | desenvolvedor | 200        | 021.346.190-07 | pedro     | 3.200,00  | m     |    3         | pj              | comissao: deve estar no padrão 1.000,00             |
        | 01/02/2020 | designer      | 250.00,00  |                | jose      | 3.200,00  | m     |    4         | pj              | cpf: deve estar no padrão 111.222.333-00            |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 |           | 3.200,00  | m     |    1         | pj              | nome: não utilizar números ou caracteres especiais  |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 | helio     |           | m     |    1         | pj              | salario: deve estar no padrão 1.000,00              |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 | helio     | 4.240,00  | masc  |    1         | pj              | sexo: m, f ou i                                     |
        | 01/02/2020 | qa            | 250.00,00  | 021.346.190-07 | helio     | 4.240,00  | m     |    0         | pj              | Departamento não cadastrado                         |
        | 01/02/2020 | gerente qa    | 250.00,00  | 021.346.190-07 | luis      | 3.200,00  | m     |    1         | pessoa física   | tipoContratacao: pj ou clt                          |