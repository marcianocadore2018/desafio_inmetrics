# language:pt

@inmetrics_desafio_api
@listar_usuario_api
Funcionalidade: Listar usuários
  Eu como profissional de QA
  Quero consultar todos os usuários cadastrados
  Para que eu possa validar se o serviço de busca está correto

  @listar_todos_usuarios_api
  Cenário: Listar todos os usuários
    Dado que estou consultando o serviço de listar usuários "empregado/list_all"
    Então o retorno do serviço da consulta deverá ser 200
    E deverei visualizar a quantidade de usuários cadastrados

  @listar_ultimo_usuario_cadastrado_api
  Cenário: Listar último usuário cadastrado
    Dado que estou consultando o último usuário cadastro no serviço "empregado/list"
    Então o retorno do serviço da consulta deverá ser 202
    E também deverei visualizar validando as informações do último usuário cadastrado