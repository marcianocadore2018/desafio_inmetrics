# language:pt

@inmetrics_desafio
@funcionario
Funcionalidade: Cadastro, Alteração, Exclusão de Funcionário
  Eu como usuário do sistema
  Quero poder realizar as operações de cadastrar, editar e excluir funcionários
  Para poder ter um controle de funcionário mais eficaz

  @cadastro_funcionario
  Cenário: Realizar cadastro do usuário com sucesso
    Dado que esteja logado no sistema
    Quando clico no menu novo funcionário
    E preencho com as informações do formulário:
      | nome                    | cpf               | cargo           | salario     | admissao      | sexo      | tipo_contratacao |
      | Marciano Luis Cadore    | 021.346.190-07    | QA              | 2000        | 13/08/2020    | Masculino | CLT              | 
    E clico no botão enviar
    Então deverei visualizar a mensagem "SUCESSO! Usuário cadastrado com sucesso"

  @editar_funcionario
  Cenário: Realizar edição do usuário pesquisando pelo usuário
    Dado que esteja logado no sistema
    Quando pesquiso pelo usuário "marciano" para realizar a alteração
    E clico no ícone de edição do primeiro usuário encontrado
    Quando altero as seguintes informações cargo "Analista de Testes" salario "14/08/2020" 
    E clico no botão enviar
    Então deverei visualizar a mensagem "SUCESSO! Informações atualizadas com sucesso" 
    E devo verificar se as informações do usuário "marciano" foram alteradas

  @excluir_funcionario
  Cenário: Realizar exclusão do usuário pesquisando pelo usuário
    Dado que esteja logado no sistema
    Quando pesquiso pelo usuário "marciano"
    E clico no ícone de exclusão do primeiro usuário da lista
    Então a exclusão deverá ser executa com sucesso exibindo a mensagem "SUCESSO! Funcionário removido com sucesso"  