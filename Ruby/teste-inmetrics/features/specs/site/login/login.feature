# language:pt

@inmetrics_desafio
@login
Funcionalidade: Login
  Eu como usuário do site
  Quero realizar login
  Para que eu possa visualizar os funcionários cadastrados no sistema

  @login_com_sucesso
  Cenário: Acesso ao sistema
    Dado que estou informando um usuário e uma senha para realizar o login
    Quando clico no botão Entre
    Então devo visualizar os funcionários cadastrados
    E quando clicar no menu sair
    Então deverei ser direcionado para a home

  @usuario_invalido
  Cenário: Usuário ou senha inválida
    Dado que estou informando um usuário e uma senha inválido para realizar o login
    Quando clico no botão Entre
    Então devo ver a mensagem "ERRO! Usuário ou Senha inválidos"