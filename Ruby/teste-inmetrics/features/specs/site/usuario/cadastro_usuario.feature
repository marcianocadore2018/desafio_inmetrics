# language:pt

@inmetrics_desafio
@usuario
Funcionalidade: Cadastrar Usuário
  Eu como usuário
  Quero poder realizar um novo cadastro
  Para que eu possa acessar o sistema

  @cadastrar_usuario
  Cenário: Cadastrar usuário com sucesso
    Dado que estou na tela de login do sistema
    Quando clico no link cadastre-se
    E realizo o cadastro do novo usuário
    Então deverei ser direcionado para a login

  @cadastrar_usuario_existente
  Cenário: Tentar realizar cadastro com usuário já existente
    Dado que estou na tela de login do sistema
    Quando clico no link cadastre-se
    E realizo o cadastro informando o usuário "testeautomacao" já cadastrado
    Então devo visualizar a mensagem "Usuário já cadastrado"