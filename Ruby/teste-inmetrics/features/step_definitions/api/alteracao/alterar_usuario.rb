
Dado('que estou consultando o id do último usuário para enviar como parâmetro na alteração') do
    @update_user_api.consult_last_register_user
end
  
E("estou informando novos dados para alteração") do
    @update_user_api.data_user
end

Dado('estou informando dados inválidos como {string} {string} {string} {string} {string} {string} {string} {int} {string}') do |admissao, cargo, comissao, cpf, nome, salario, sexo, departamento, tipocontratacao|
    @update_user_api.data_user_invalid(admissao, cargo, comissao, cpf, nome, salario, sexo, departamento, tipocontratacao)  
end


Quando('envio a solicitação de alteração no serviço {string}') do |url|
    @update_user_api.put_user(url)
end

E("a mensagem de alteração deverá ser {string}") do |message|
    expect(@update_user_api.response_data).to include message
end

Então("o retorno do serviço da alteração deverá ser {int}") do |code|
    expect(@update_user_api.response_code).to eq code 
end