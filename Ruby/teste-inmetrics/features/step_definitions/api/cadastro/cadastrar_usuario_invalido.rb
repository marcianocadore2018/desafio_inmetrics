Dado('que estou enviando as dados inválidos {string} {string} {string} {string} {string} {string} {string} {int} {string}') do |admissao, cargo, comissao, cpf, nome, salario, sexo, departamento, tipocontratacao|
    @new_user_api.data_new_user_invalid(admissao, cargo, comissao, cpf, nome, salario, sexo, departamento, tipocontratacao)
end

Então('a mensagem deverá ser {string}') do |message|
    expect(@new_user_api.response_data).to include message
end