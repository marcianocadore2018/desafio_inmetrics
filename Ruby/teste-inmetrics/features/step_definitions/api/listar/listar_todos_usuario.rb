Dado('que estou consultando o serviço de listar usuários {string}') do |url|
    @consult_user_api.consult(url)
end

Então("o retorno do serviço da consulta deverá ser {int}") do |code|
    expect(@consult_user_api.response_code).to eq code
end
  
E('também deverei visualizar validando as informações do último usuário cadastrado') do
    @last_user_added = @consult_user_api.consult_last_user
    expect(@last_user_added["empregadoId"]).not_to be nil
    expect(@last_user_added["nome"]).not_to be nil
    expect(@last_user_added["sexo"]).not_to be nil
    expect(@last_user_added["cpf"]).not_to be nil
    expect(@last_user_added["cargo"]).not_to be nil
    expect(@last_user_added["admissao"]).not_to be nil
    expect(@last_user_added["salario"]).not_to be nil
    expect(@last_user_added["comissao"]).not_to be nil
    expect(@last_user_added["tipoContratacao"]).not_to be nil
end

E("deverei visualizar a quantidade de usuários cadastrados") do
    @consult_user_api.quantity_all_user
end