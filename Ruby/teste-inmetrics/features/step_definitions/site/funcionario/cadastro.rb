Dado('que esteja logado no sistema') do
    steps %{
        Dado que estou informando um usuário e uma senha para realizar o login
        Quando clico no botão Entre
    }
end
  
Quando('clico no menu novo funcionário') do
   @home.click_new_employees
end

E('preencho com as informações do formulário:') do |table|
   data = table.hashes[0]
   @new_employees.fill_form_employes(data)
end

Quando('clico no botão enviar') do
   @new_employees.click_btn_form
end

Então('deverei visualizar a mensagem {string}') do |message|
    expect(@new_employees.message_success.text).to include message
end