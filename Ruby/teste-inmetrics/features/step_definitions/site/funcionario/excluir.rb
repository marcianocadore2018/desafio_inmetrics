Quando('pesquiso pelo usuário {string}') do |name|
    @home.search_name_employee(name)
end

Quando('clico no ícone de exclusão do primeiro usuário da lista') do
    @home.delete_user
end

Então('a exclusão deverá ser executa com sucesso exibindo a mensagem {string}') do |message|
    expect(@delete_employees.message_success.text).to include message
end