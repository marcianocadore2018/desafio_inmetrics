Dado("que estou informando um usuário e uma senha para realizar o login") do
  @login.home
  user_login = @login.load_data_test['user_login']
  @login.login_user(user_login)
end
  
Quando('clico no botão Entre') do
  @login.button_login
end

Então('devo visualizar os funcionários cadastrados') do
  expect(@home.table_employees).to be_truthy
end
  
Então('quando clicar no menu sair') do
  @home.logout
end
  
Então('deverei ser direcionado para a home') do
  expect(@login.title_page.text).to eql "Login"
end
  