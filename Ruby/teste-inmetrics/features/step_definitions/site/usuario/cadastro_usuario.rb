Dado('que estou na tela de login do sistema') do
    @login.home
    expect(@login.title_page.text).to eql("Login")
end

Quando('clico no link cadastre-se') do
    @login.register
end

Então('realizo o cadastro do novo usuário') do
    @new_user.fill_form_user
end

Então('deverei ser direcionado para a login') do
    expect(@login.title_page.text).to eql("Login")
    expect(page.current_path).to eql("/accounts/login/")
end