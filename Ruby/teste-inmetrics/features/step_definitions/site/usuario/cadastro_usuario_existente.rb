Então('realizo o cadastro informando o usuário {string} já cadastrado') do |user|
    @new_user.fill_form_user_existent(user)
end
  
Então('devo visualizar a mensagem {string}') do |string|
    expect(page).to have_content("Usuário já cadastrado")
end