require "base64"

Before do
  @login = LoginPage.new
  @home = HomePage.new
  @new_employees = NewEmployeesPage.new
  @delete_employees = DeleteEmployeesPage.new
  @edit_employees = EditEmployeesPage.new
  @new_user = NewUserPage.new

  #pages_api
  @new_user_api = NewUserApi.new
  @update_user_api = UpdateUserApi.new
  @consult_user_api = ConsultUserApi.new
end